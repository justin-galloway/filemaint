﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DataAccessLibrary.Models
{
    public class OrderModel
    {
        public string ORD { get; set; }
        public string CUST { get; set; }
        public string SVIA { get; set; }
    }
}
