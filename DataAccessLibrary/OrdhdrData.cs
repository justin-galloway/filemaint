﻿using Dapper;
using DataAccessLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    public class OrdhdrData
    {
        private readonly SqlDataAccess _db;
        public OrdhdrData(SqlDataAccess db)
        {
            _db = db;
        }
        public Task<List<OrderModel>> GetOrder()
        {
            string sql = "select ORD, CUST, SVIA from ORDHDR where CUST='7AZ006'";
            return _db.LoadData<OrderModel>(sql);
        }
    }
}
